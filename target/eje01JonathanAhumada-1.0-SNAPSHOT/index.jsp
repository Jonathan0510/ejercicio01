<%-- 
    Document   : index
    Created on : 29-03-2021, 10:45:38
    Author     : Jonathan Ahumada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pagina de Inicio</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilostablas.css" type="text/css" media="all" />
        <!--  -->
       <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    
        <body>
        <form name="form" action="ingreso" method="POST" onsubmit="return validacion();">
        <center>
        <h2 class="text-primary">FORMULARIO DE REGISTRO</h2>
	<br>
	<table >
                <td><br></td>
           	<tr><td colspan="1"></td></tr>
                <tr><td><label>NOMBRE<br></label></td><td><input type="text" id="nombre" name="nombre" class="form-control" placeholder="JUAN" ></td></tr>
                <tr><td><label>APELLIDO<br></label></td><td><input type="text" id="apellido" name="nombre" class="form-control" placeholder="DIAZ PEREZ" ></td></tr>
                <tr><td><label>CORREO ELECTRONICO</label></td><td><input type="email" id="email" class="form-control" name="email" placeholder="juan.diaz@ciisa.cl"></td></tr>
                <tr><td><label>DIRECCION<br></label></td><td><input type="text" id="direccion" name="nombre" class="form-control" placeholder="HIERRO VIEJO 411" ></td></tr>
                <tr><td><label>SECCION</label></td><td><select id="seccion" name="seccion">
                            <option></option>
                            <option>50</option>
                            <option>51</option>
                            <option>52</option>
                            <option>53</option>
                </select></td>
                </tr>    
                <td><br></td>
	</table>
        <br> 
    <button type="submit" name="envio" value="registro" class="btn btn-success">Registrar</button>
	
</center>
	</form>
            </body>
           
</html>
